require 'spec_helper'
require 'api/runners'

module GitLab
  module API
    describe RunnersClient do
      let(:runner1) { { id: '1', description: 'runner-1', active: true,
                        is_shared: true, contacted_at: '2017-05-09 23:30:57 +0000',
                        version: '9.1.0', tag_list: ['shell'] } }
      let(:runner2) { { id: '2', description: 'runner-2', active: false,
                        is_shared: true, contacted_at: '2017-05-09 23:30:57 +0000',
                        version: '9.0.2', tag_list: ['docker'] } }
      let(:runner3) { { id: '3', description: 'runner-3', active: true,
                        is_shared: false, contacted_at: '2017-05-09 23:30:57 +0000',
                        version: '9.1.0', tag_list: ['shell'] } }
      let(:runners) { [runner1, runner2, runner3]}

      describe '#list' do
        context 'when runners are available' do
          it 'returns list of active runners' do
            req = stub_request(:get, /api\/v4\/runners\/all\?scope=shared/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 200, body: runners.select{|r| r[:is_shared]}.to_json } }

            found_runners = subject.list.to_a
            expect(found_runners.count).to eq(2)
            expect(found_runners).to include(runner1)
            expect(found_runners).to include(runner2)
            expect(found_runners).not_to include(runner3)

            remove_request_stub(req)
          end
        end

        context 'when there is no runners' do
          it 'returns empty list' do
            req = stub_request(:get, /api\/v4\/runners/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 200, body: [].to_json } }

            found_runners = subject.list.to_a
            expect(found_runners.count).to eq(0)

            remove_request_stub(req)
          end
        end

        context 'when the server errors out' do
          it 'fails with an error' do
            req = stub_request(:get, /api\/v4\/runners/).
                  with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                  to_return lambda {|request| { status: 500 } }

            expect{subject.list}.to raise_error(/Failed to list runners: 500/)

            remove_request_stub(req)
          end
        end
      end

      describe '#info' do
        context 'when runner exists' do
          it 'returns runner details' do
            req = stub_request(:get, /api\/v4\/runners\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 200, body: runner1.to_json } }

            found_runners = subject.info(runner1[:id])
            expect(found_runners.to_h).to eq(runner1)

            remove_request_stub(req)
          end
        end

        context 'when no runner ID is provided' do
          it 'fails with an error' do
            expect{subject.info('')}.to raise_error(/Runner's ID is required/)
          end
        end

        context 'when the server errors out' do
          it 'fails with an error' do
            req = stub_request(:get, /api\/v4\/runners\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 500 } }

            expect{subject.info('1')}.to raise_error(/Failed to show details of Runner 1: 500/)

            remove_request_stub(req)
          end
        end
      end

      describe '#pause' do
        it 'mark runner as paused' do
          sentinel = { active: runner1[:active] }
          req = stub_request(:put, /api\/v4\/runners\/.*/).
              with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
              to_return lambda { |request|
                sentinel[:active] = false
                { status: 200, body: runner1.to_json }
              }

          subject.pause(runner1[:id])
          expect(sentinel[:active]).to be_falsey

          remove_request_stub(req)
        end

        context 'when no runner ID is provided' do
          it 'fails with an error' do
            expect{subject.pause('')}.to raise_error(/Runner's ID is required/)
          end
        end

        context 'when the server errors out' do
          it 'fails with an error' do
            req = stub_request(:put, /api\/v4\/runners\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 500 } }

            expect{subject.pause('1')}.to raise_error(/Failed to pause Runner 1: 500/)

            remove_request_stub(req)
          end
        end
      end

      describe '#unpause' do
        it 'mark runner as unpaused' do
          sentinel = { active: runner2[:active] }
          req = stub_request(:put, /api\/v4\/runners\/.*/).
              with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
              to_return lambda { |request|
                sentinel[:active] = true
                { status: 200, body: runner2.to_json }
              }

          subject.pause(runner2[:id])
          expect(sentinel[:active]).to be_truthy

          remove_request_stub(req)
        end

        context 'when no runner ID is provided' do
          it 'fails with an error' do
            expect{subject.unpause('')}.to raise_error(/Runner's ID is required/)
          end
        end

        context 'when the server errors out' do
          it 'fails with an error' do
            req = stub_request(:put, /api\/v4\/runners\/.*/).
                with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
                to_return lambda {|request| { status: 500 } }

            expect{subject.unpause('1')}.to raise_error(/Failed to unpause Runner 1: 500/)

            remove_request_stub(req)
          end
        end
      end
    end
  end
end
