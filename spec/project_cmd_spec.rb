require 'spec_helper'
require 'cog_cmd/gitlab/project/find'

describe "find project command" do
  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  it "Works to not find a project" do
    req = stub_request(:get, /api\/v4\/projects\/.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return( status: 404 )

    with_environment("group/project") {
      finder = CogCmd::Gitlab::Project::Find.new
      expect { finder.run_command }.to raise_error(/Could not find project with path group\/project/)
      # expect(finder.response.content).to be_empty
    }

    remove_request_stub(req)
  end

  it "Works to find a project" do
    req = stub_request(:get, /api\/v4\/projects\/.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return lambda {|request| { status: 200, body: {id: 1, name: "my project"}.to_json } }

    with_environment("group/project") {
      finder = CogCmd::Gitlab::Project::Find.new
      finder.run_command
      expect(finder.response.content[0][:name]).to eq("my project")
    }

    remove_request_stub(req)
  end
end

