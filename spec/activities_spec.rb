require "spec_helper"
require "api/activities"

module GitLab
  module API
    describe ActivitiesClient do
      describe "#get" do
        it "fails if no date is sent" do
          expect { subject.get(nil) }.to raise_error /Date from is required/
          expect { subject.get("") }.to raise_error /Date from is required/
        end

        it "errs with a valid error message on failure" do
          req = stub_request(:get, /api\/v4\/user\/activities.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 401, body: "unauthorized" )

          expect { subject.get(Date.today.to_s) }.to raise_error /Failed to get active users from #{Date.today.to_s}: 401 - unauthorized/

          remove_request_stub(req)
        end
      end
    end
  end
end
