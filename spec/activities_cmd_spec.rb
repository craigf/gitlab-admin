require 'spec_helper'
require 'cog_cmd/gitlab/activities'

describe "activities" do
  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  it "Returns the total number for today if no date is sent" do
    req = stub_request(:get, /api\/v4\/user\/activities.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return( status: 200, headers: { "X-Total" => 1024 } )

    with_environment(Date.today.to_s) {
      activities = CogCmd::Gitlab::Activities.new
      activities.run_command
      expect(activities.response.content).to eq("1024 total active users from #{Date.today.to_s}")
    }

    remove_request_stub(req)
  end
end
