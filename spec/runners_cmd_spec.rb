require 'spec_helper'
require 'cog_cmd/gitlab/runners/info'
require 'cog_cmd/gitlab/runners/list'
require 'cog_cmd/gitlab/runners/pause'
require 'cog_cmd/gitlab/runners/unpause'

describe 'runners commands' do
  let(:runner1) { { id: '1', description: 'runner-1', active: true,
                    is_shared: false, contacted_at: '2017-05-09 23:30:57 +0000',
                    version: '9.1.0', tag_list: ['shell'] } }
  let(:runner2) { { id: '2', description: 'runner-2', active: false,
                    is_shared: true, contacted_at: '2017-05-09 23:30:57 +0000',
                    version: '9.0.2', tag_list: ['docker'] } }
  let(:runners) { [runner1, runner2]}

  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  describe 'list command' do
    context 'when runners are available' do
      it 'returns a list of runners' do
        req = stub_request(:get, /api\/v4\/runners/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda {|request| { status: 200, body: runners.to_json } }

        list = CogCmd::Gitlab::Runners::List.new
        list.run_command

        content = list.response.content
        expect(content.count).to eq(2)
        expect(content).to include(runner1)
        expect(content).to include(runner2)
        expect(list.response.template).to eq('runners_list')

        remove_request_stub(req)
      end
    end

    context 'when there is no runners' do
      it 'returns an empty list' do
        req = stub_request(:get, /api\/v4\/runners/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda {|request| { status: 200, body: [].to_json } }

        list = CogCmd::Gitlab::Runners::List.new
        list.run_command

        expect(list.response.content).to be_empty
        expect(list.response.template).to eq('runners_list')

        remove_request_stub(req)
      end
    end
  end

  describe 'info command' do
    it 'shows runner details' do
      req = stub_request(:get, /api\/v4\/runners\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request| { status: 200, body: runner1.to_json } }

      with_environment(runner1[:id]) {
        getter = CogCmd::Gitlab::Runners::Info.new
        getter.run_command

        expect(getter.response.content).to eq(runner1)
        expect(getter.response.template).to eq('runners_info')
      }

      remove_request_stub(req)
    end

    context 'when ID argument is not present' do
      it 'raises exception' do
        getter = CogCmd::Gitlab::Runners::Info.new
        expect{ getter.run_command }.to raise_error(/I need a Runner ID to get Runner's details/)
      end
    end
  end

  describe 'pause command' do
    it 'marks runner as paused' do
      req = stub_request(:put, /api\/v4\/runners\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request|
            runner1[:active] = false
            { status: 200, body: runner1.to_json }
          }

      with_environment(runner1[:id]) {
        pauser = CogCmd::Gitlab::Runners::Pause.new
        pauser.run_command

        content = pauser.response.content
        expect(content[:active]).to be_falsey
        expect(content).to eq(runner1)
        expect(pauser.response.template).to eq('runners_active_state_change')
      }

      remove_request_stub(req)
    end

    context 'when ID argument is not present' do
      it 'raises exception' do
        pauser = CogCmd::Gitlab::Runners::Pause.new
        expect{ pauser.run_command }.to raise_error(/I need a Runner ID to pause a Runner/)
      end
    end
  end

  describe 'unpause command' do
    it 'marks runner as unpaused' do
      req = stub_request(:put, /api\/v4\/runners\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request|
            runner2[:active] = true
            { status: 200, body: runner2.to_json }
          }

      with_environment(runner2[:id]) {
        unpauser = CogCmd::Gitlab::Runners::Unpause.new
        unpauser.run_command

        content = unpauser.response.content
        expect(content[:active]).to be_truthy
        expect(content).to eq(runner2)
        expect(unpauser.response.template).to eq('runners_active_state_change')
      }

      remove_request_stub(req)
    end

    context 'when ID argument is not present' do
      it 'raises exception' do
        unpauser = CogCmd::Gitlab::Runners::Unpause.new
        expect{ unpauser.run_command }.to raise_error(/I need a Runner ID to unpause a Runner/)
      end
    end
  end
end
