require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Block < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username is required to block")
          GitLab::API::UserClient.new.block(username)
          response.content = "User #{username} has been blocked"
        end
      end
    end
  end
end
