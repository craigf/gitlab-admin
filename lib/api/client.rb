require "cgi"
require "json"
require "uri"
require "net/http"

module GitLab
  module API
    class Client
      attr_accessor :url, :token

      def initialize(url: nil, token: nil)
        @url = (url || ENV.fetch("GITLAB_URL", "https://gitlab.com"))
        @token = token || ENV.fetch("GITLAB_TOKEN", "no-token")
      end

      def get(path)
        http_request(path) { |uri| Net::HTTP::Get.new(uri) }
      end

      def post(path, values = {})
        http_request(path) { |uri|
          post_request = Net::HTTP::Post.new(uri)
          post_request.body = values.map { |key, value| "#{key}=#{value}" }.join("&")
          post_request
        }
      end

      def put(path, values = {})
        http_request(path) { |uri|
          post_request = Net::HTTP::Put.new(uri)
          post_request.body = values.map { |key, value| "#{key}=#{value}" }.join("&")
          post_request
        }
      end

      private

      def http_request(path)
        # Remove leading slashes from path to avoid a 308 Permanent Redirect
        path.sub!(%r{\A/+}, "")

        uri = URI.parse("#{@url}/#{path}")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == "https"
        request = yield uri
        request["PRIVATE-TOKEN"] = @token
        http.request(request)
      end
    end
  end
end

class Struct
  def self.from_hash(clnt)
    new(*clnt.values_at(*members.map(&:to_s)))
  end
end
