require_relative "client"

module GitLab
  module API
    Project = Struct.new(:id, :public, :default_branch, :name, :name_with_namespace,
                         :path, :path_with_namespace, :web_url, :created_at,
                         :last_activity_at, :archived, :star_count, :forks_count,
                         :repository_storage, :description, :container_registry_enabled,
                         :issues_enabled, :merge_requests_enabled, :wiki_enabled,
                         :jobs_enabled, :snippets_enabled, :shared_runners_enabled,
                         :lfs_enabled, :public_jobs, :request_access_enabled)

    class ProjectFinder
      def initialize(project_path, client)
        fail Cog::Error.new("A project path is required") if project_path.nil? || project_path.empty?
        @project_path = project_path
        @client = client
      end

      def find
        response = @client.get("/api/v4/projects/#{CGI.escape(@project_path)}")
        project = case response.code.to_i
                  when 200
                    if response.body.strip.empty?
                      nil
                    else
                      JSON.parse(response.body)
                    end
                  when 404
                    nil
                  else
                    fail Cog::Error.new("Failed to get project #{@project_path}: " \
                                        "#{response.code} - #{response.message}")
                  end
        build(project)
      end

      def build(payload)
        fail Cog::Error.new("Could not find project with path #{@project_path}") if payload.nil?
        Project.from_hash(payload)
      end
    end

    class ProjectClient
      def initialize(client = Client.new)
        @client = client
      end

      def find(project_path)
        project = ProjectFinder.new(project_path, @client).find
        if block_given?
          yield project
        else
          project
        end
      end
    end
  end
end
